package com.hyper.hyper_recipes_ines.model;

/**
 * Created by Ines on 20/10/2015.
 */
public enum Difficulty {
    EASY(1),
    MEDIUM(2),
    HARD(3);

    private int id;

    private Difficulty(int id) {
        this.id = id;
    }

    public static Difficulty getType(Integer id) {

        if (id == null) {
            return null;
        }

        for (Difficulty position : Difficulty.values()) {
            if (id.equals(position.getId())) {
                return position;
            }
        }
        throw new IllegalArgumentException("No matching type for id " + id);
    }

    public int getId() {
        return id;
    }
}
