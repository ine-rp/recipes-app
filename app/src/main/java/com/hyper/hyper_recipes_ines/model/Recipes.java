package com.hyper.hyper_recipes_ines.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Ines on 20/10/2015.
 */
public class Recipes implements Parcelable {

    private long id;
    private String name;
    private String description;
    private String instructions;
    private boolean favorite;
    private int difficulty;
    private String createdAt;
    private String updatedAt;
    private Photo photo;

    public Recipes(){
        super();
    }

    public Recipes(long id, String name, String description, String instructions, boolean favorite, int difficulty, String createdAt, String updatedAt, Photo photo) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.instructions = instructions;
        this.favorite = favorite;
        this.difficulty = difficulty;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        this.photo = photo;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getInstructions() {
        return instructions;
    }

    public void setInstructions(String instructions) {
        this.instructions = instructions;
    }

    public boolean isFavorite() {
        return favorite;
    }

    public void setFavorite(boolean favorite) {
        this.favorite = favorite;
    }

    public int getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(int difficulty) {
        this.difficulty = difficulty;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Photo getPhoto() {
        return photo;
    }

    public void setPhoto(Photo photo) {
        this.photo = photo;
    }


    // Parcelling
    public Recipes(Parcel in){
        id = in.readLong();
        name = in.readString();
        description = in.readString();
        instructions = in.readString();
        favorite = (in.readInt() == 1);
        difficulty = in.readInt();
        createdAt = in.readString();
        updatedAt = in.readString();
        photo = (Photo) in.readValue(Photo.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.id);
        dest.writeString(this.name);
        dest.writeString(this.description);
        dest.writeString(this.instructions);
        dest.writeInt(this.favorite ? 1 : 0);
        dest.writeInt(this.difficulty);
        dest.writeString(this.createdAt);
        dest.writeString(this.updatedAt);
        dest.writeValue(this.photo);
    }
    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public Recipes createFromParcel(Parcel in) {
            return new Recipes(in);
        }

        public Recipes[] newArray(int size) {
            return new Recipes[size];
        }
    };

}
