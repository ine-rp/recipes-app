package com.hyper.hyper_recipes_ines.views;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hyper.hyper_recipes_ines.R;

/**
 * Created by Ines on 17/10/2015.
 */
public class NewRecipeFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_new_recipe,container,false);
        return v;
    }
}
