package com.hyper.hyper_recipes_ines.views;

import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.hyper.hyper_recipes_ines.R;
import com.hyper.hyper_recipes_ines.model.Recipes;
import com.hyper.hyper_recipes_ines.services.DataService;
import com.hyper.hyper_recipes_ines.services.RestService;
import com.hyper.hyper_recipes_ines.utils.Constants;
import com.squareup.picasso.Picasso;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Ines on 22/10/2015.
 */
public class RecipeDetailsActivity extends AppCompatActivity {

    @Bind(R.id.recipe_details_toolbar) Toolbar toolbar;
    @Bind(R.id.recipe_details_collapsing_toolbar) CollapsingToolbarLayout collapsingToolbar;
    @Bind(R.id.recipe_details_image) ImageView topImage;
    @Bind(R.id.recipe_details_favorite) FloatingActionButton favoriteButton;
    @Bind(R.id.recipe_details_description) TextView description;
    @Bind(R.id.recipe_details_instructions) TextView instructions;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recipe_details);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Bundle data = getIntent().getExtras();
        Recipes recipe = (Recipes) data.getParcelable(Constants.INTENT_RECIPES_FRAGMENT_RECIPE);

        // change satus bar color when collapsing
        collapsingToolbar.setStatusBarScrimColor(getResources().getColor(R.color.primary_dark_color));
        collapsingToolbar.setTitle(recipe.getName());
        Picasso.with(this).load(recipe.getPhoto().getUrl()).placeholder(R.drawable.foodbackground).into(topImage);
        description.setText(recipe.getDescription());
        instructions.setText(recipe.getInstructions());
        if (recipe.isFavorite()){
            favoriteButton.setImageResource(R.drawable.ic_favorite_white_24dp);
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
