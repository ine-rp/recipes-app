package com.hyper.hyper_recipes_ines.views;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.hyper.hyper_recipes_ines.R;
import com.hyper.hyper_recipes_ines.adapters.RecipesListAdapter;
import com.hyper.hyper_recipes_ines.listeners.RecyclerItemClickListener;
import com.hyper.hyper_recipes_ines.model.Photo;
import com.hyper.hyper_recipes_ines.model.Recipes;
import com.hyper.hyper_recipes_ines.presenters.RecipesPagePresenter;
import com.hyper.hyper_recipes_ines.utils.BusProvider;
import com.hyper.hyper_recipes_ines.utils.Constants;
import com.hyper.hyper_recipes_ines.utils.DividerItemDecoration;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit.RetrofitError;

/**
 * Created by Ines on 17/10/2015.
 */
public class RecipesFragment extends Fragment {

    private RecyclerView recipesList;
    private RecipesPagePresenter recipesPresenter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        recipesPresenter = new RecipesPagePresenter(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        BusProvider.getUIBusInstance().register(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_recipes,container,false);
        recipesList = (RecyclerView) v.findViewById(R.id.recipes_list);

        recipesPresenter.getRecipes();

        return v;
    }

    @Subscribe
    public void setRecipesList(final Recipes[] recipes) {
        // set Recipes RecyclerView
        recipesList.addItemDecoration(new DividerItemDecoration(getActivity()));
        recipesList.setAdapter(new RecipesListAdapter(recipes, R.layout.recipe_item));
        recipesList.setLayoutManager(new LinearLayoutManager(getActivity()));
        recipesList.setItemAnimator(new DefaultItemAnimator());
        recipesList.setHasFixedSize(true);

        recipesList.addOnItemTouchListener(
                new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
                    @Override public void onItemClick(View view, int position) {
                        Recipes selectedRecipe = recipes[position];
                        Intent myIntent = new Intent(getActivity(), RecipeDetailsActivity.class);
                        myIntent.putExtra(Constants.INTENT_RECIPES_FRAGMENT_RECIPE, selectedRecipe);
                        startActivity(myIntent);
                    }
                })
        );
    }

    @Subscribe
    public void errorGettingRecipes(RetrofitError error) {
        Toast.makeText(getActivity(), "Error retrieving Recipes", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onPause() {
        BusProvider.getUIBusInstance().unregister(this);
        super.onPause();
    }

}
