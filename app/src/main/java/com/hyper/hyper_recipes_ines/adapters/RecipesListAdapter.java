package com.hyper.hyper_recipes_ines.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.hyper.hyper_recipes_ines.R;
import com.hyper.hyper_recipes_ines.model.Difficulty;
import com.hyper.hyper_recipes_ines.model.Recipes;
import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import java.util.List;

/**
 * Created by Ines on 18/10/2015.
 */
public class RecipesListAdapter extends RecyclerView.Adapter<RecipesListAdapter.ViewHolder>{

    private Recipes[] items;
    private int itemLayout;

    public RecipesListAdapter(Recipes[] items, int itemLayout) {
        this.items = items;
        this.itemLayout = itemLayout;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(itemLayout, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Recipes recipe = items[position];
        holder.name.setText(recipe.getName());
        holder.difficulty.setText(Difficulty.getType(recipe.getDifficulty()).toString());
        Picasso.with(holder.image.getContext()).load(recipe.getPhoto().getThumbnailUrl()).placeholder(R.drawable.foodbackground).into(holder.image);
        holder.itemView.setTag(recipe);
    }

    @Override
    public int getItemCount() {
        return items.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView image;
        public TextView name;
        public TextView difficulty;

        public ViewHolder(View itemView) {
            super(itemView);
            image = (ImageView) itemView.findViewById(R.id.recipe_image);
            name = (TextView) itemView.findViewById(R.id.recipe_name);
            difficulty = (TextView) itemView.findViewById(R.id.recipe_difficulty);
        }
    }
}
