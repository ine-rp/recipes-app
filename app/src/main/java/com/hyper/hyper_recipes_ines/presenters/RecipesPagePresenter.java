package com.hyper.hyper_recipes_ines.presenters;

import com.hyper.hyper_recipes_ines.services.DataService;
import com.hyper.hyper_recipes_ines.services.RestService;
import com.hyper.hyper_recipes_ines.views.RecipesFragment;

/**
 * Created by Ines on 21/10/2015.
 */
public class RecipesPagePresenter {
    private RecipesFragment recipesPageView;

    public RecipesPagePresenter (RecipesFragment view) {
        this.recipesPageView = view;
    }

    public void getRecipes() {
        final DataService dataService = RestService.getInstance();
        dataService.getRecipes();
    }
}
