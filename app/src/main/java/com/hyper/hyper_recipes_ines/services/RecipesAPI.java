package com.hyper.hyper_recipes_ines.services;

import com.hyper.hyper_recipes_ines.model.Recipes;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Headers;

/**
 * Created by Ines on 21/10/2015.
 */
public interface RecipesAPI {

    /**
     * Retrieves recipes list.
     *
     * @param cb Callback to be executed when the response finishes.
     */
    @Headers("Content-type: application/json")
    @GET("/recipes")
    void getRecipes(Callback<Recipes[]> cb);

}