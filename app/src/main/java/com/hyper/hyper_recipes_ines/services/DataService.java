package com.hyper.hyper_recipes_ines.services;

/**
 * Created by Ines on 21/10/2015.
 */
public interface DataService {

    void getRecipes();
}
