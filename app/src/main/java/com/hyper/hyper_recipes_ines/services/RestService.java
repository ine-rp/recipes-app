package com.hyper.hyper_recipes_ines.services;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.hyper.hyper_recipes_ines.model.Recipes;
import com.hyper.hyper_recipes_ines.utils.BusProvider;
import com.hyper.hyper_recipes_ines.utils.Constants;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.otto.Subscribe;

import retrofit.Callback;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.OkClient;
import retrofit.client.Response;
import retrofit.converter.GsonConverter;

import java.util.concurrent.TimeUnit;

/**
 * Created by Ines on 21/10/2015.
 */
public class RestService implements DataService {
    public static RestService INSTANCE;
    private final RecipesAPI recipesAPI;

    private RestService() {
        // HTTP

        //Create JSON parser
        final Gson gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd HH:mm:ss")
                .create();

        final OkHttpClient okHttpClient = new OkHttpClient();
        okHttpClient.setConnectTimeout(Constants.API_CONNECT_TIMEOUT, TimeUnit.SECONDS);
        okHttpClient.setWriteTimeout(Constants.API_WRITE_TIMEOUT, TimeUnit.SECONDS);
        okHttpClient.setReadTimeout(Constants.API_READ_TIMEOUT, TimeUnit.SECONDS);


        RestAdapter.Builder builder = new RestAdapter.Builder()
                .setClient(new OkClient(okHttpClient))
                .setEndpoint(Constants.API_BASE_ENDPOINT)
                .setConverter(new GsonConverter(gson))
                .setLogLevel(RestAdapter.LogLevel.FULL);

        builder.setRequestInterceptor(new RequestInterceptor() {
            @Override
            public void intercept(RequestInterceptor.RequestFacade request) {
                request.addHeader("Authorization", "Token " + Constants.API_TOKEN);
            }
        });

        RestAdapter restAdapter = builder.build();
        recipesAPI = restAdapter.create(RecipesAPI.class);
    }

    public static RestService getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new RestService();
        }

        return INSTANCE;
    }

    @Override
    public void getRecipes() {
        recipesAPI.getRecipes(new Callback<Recipes[]>() {
            @Subscribe
            @Override
            public void success(Recipes[] recipes, Response response) {
                BusProvider.getUIBusInstance().post(recipes);
            }

            @Subscribe
            @Override
            public void failure(RetrofitError error) {
                BusProvider.getUIBusInstance().post(error);
            }
        });
    }
}
