package com.hyper.hyper_recipes_ines.utils;

/**
 * Created by Ines on 21/10/2015.
 */
public class Constants {

    // API
    public static final String API_BASE_ENDPOINT = "http://hyper-recipes.herokuapp.com/";
    public static final long API_CONNECT_TIMEOUT = 6;
    public static final long API_WRITE_TIMEOUT = 7;
    public static final long API_READ_TIMEOUT = 9;
    public static final String API_TOKEN = "f86f584db8990e19229e";

    // Intents
    public static final String INTENT_RECIPES_FRAGMENT_RECIPE = "recipes_fragment_recipe";

}
